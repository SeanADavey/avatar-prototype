﻿using UnityEngine;
using System.Collections;

public class BlendshapeSlider : MonoBehaviour {

	public string postURL = "http://localhost/testing/SaveScore.php";

	public float height = 50.0F;
	public float heightPrevious = 50.0f;
	public float neck = 50.0F;
	public float shoulders = 50.0F;
	public float chest = 50.0F;
	public float waist = 50.0F;
	public float hips = 50.0F;
	public float inseam = 50.0F;
    public float thighs = 50.0f;
    public float biceps = 50.0f;

	public string heightInput;


	public string centi = "cms";

	public float Body_Height_Min = 150.0F;

	public float Body_Height_Max = 214.0F;

	private SkinnedMeshRenderer skinMeshRenderer;
	public GUIStyle  Style;
    public GUIStyle Title;



	public string stringToEdit = "";



	// Use this for initialization
	void Start () {
		
		skinMeshRenderer = GetComponent<SkinnedMeshRenderer>();
		Style.normal.textColor = Color.white;
        Style.fontSize = 20;
        Title.fontSize = 30;
        Title.normal.textColor = Color.white;
        GUI.backgroundColor = Color.white;
		Style.alignment = TextAnchor.MiddleCenter;
        
        /*
        //sliderFill.normal.textColor = Color.white;     
        //string fillTex = "Textures/slider";
        string thumbTex = "Textures/thumb";
        //sliderFill.normal.background = (Texture2D)Resources.Load(fillTex, typeof(Texture2D));
        sliderThumb.normal.background = (Texture2D)Resources.Load(thumbTex, typeof(Texture2D));
        //sliderFill.fixedHeight = 8;
        //sliderFill.padding = new RectOffset(5, 5, 0, 5);
        sliderThumb.padding = new RectOffset(5, 5, 0, 5);
        sliderThumb.fixedHeight = 12;
        sliderThumb.fixedWidth = 12;
        */






    }

    void OnGUI()
	{
		

        /*Initialising GUI elements*/

        //Title 
        GUI.Label(new Rect(60, 100, 200, 50), "Set up your avatar", Title);


        //BodyWeight_FullNumber = (BodyWeight_Slider);
		heightInput = GUI.TextArea (new Rect (290, 290, 40, 22), heightInput, Style);
		height = GUI.HorizontalSlider(new Rect(80 , 230 , 200 , 30) , height , 1.0F , 99.0F);
		GUI.Label(new Rect(150 , 205 , 200 , 20), "Height" , Style);

		//BodyNeck_FullNumber= Mathf.Round(Neck);
		//GUI.Box (new Rect (370, 290, 100, 30), (BodyNeck_FullNumber).ToString() + centi);
		neck = GUI.HorizontalSlider(new Rect(80 , 295 , 200 , 30) , neck , 1.0F , 99.0F);
		GUI.Label(new Rect(155 , 270 , 200 , 20), "Neck" , Style);

		//BodyShoulders_FullNumber= Mathf.Round(BodyShoulders_Slider);
		//GUI.Box (new Rect (100, 275, 100, 30), BodyShoulders_FullNumber.ToString() + centi);
		shoulders = GUI.HorizontalSlider(new Rect(80, 350 , 200 , 30) , shoulders , 1.0F , 99.0F);
		GUI.Label(new Rect(135 , 325 , 200 , 20), "Shoulders" , Style);

		//BodyChest_FullNumber= Mathf.Round(BodyChest_Slider);
		//GUI.Box (new Rect (100, 375, 100, 30), BodyChest_FullNumber.ToString() + centi);
		chest = GUI.HorizontalSlider(new Rect(80, 415 , 200 , 30) , chest , 1.0F , 99.0F);
		GUI.Label(new Rect(150 , 390 , 200 , 20), "Chest" , Style);

		//BodyWaist_FullNumber= Mathf.Round(BodyWaist_Slider);
		//GUI.Box (new Rect (700, 75, 100, 30), BodyWaist_FullNumber.ToString() + centi);
		waist = GUI.HorizontalSlider(new Rect(80, 480 , 200 , 30) , waist , 1.0F , 99.0F);
		GUI.Label(new Rect(150 , 455 , 200 , 20), "Waist" , Style);

		//BodyHips_FullNumber= Mathf.Round(BodyHips_Slider);
		//GUI.Box (new Rect (700, 175, 100, 30), BodyHips_FullNumber.ToString() + centi);
		hips = GUI.HorizontalSlider(new Rect(80, 545 , 200 , 30) , hips , 1.0F , 99.0F);
		GUI.Label(new Rect(160 , 520 , 200 , 20), "Hips" , Style);

		//BodyInseam_FullNumber= Mathf.Round(BodyInseam_Slider);
		//GUI.Box (new Rect (700, 275, 100, 30), BodyInseam_FullNumber.ToString() + centi);
		inseam = GUI.HorizontalSlider(new Rect(80, 610 , 200 , 30) , inseam , 1.0F , 99.0F);
		GUI.Label(new Rect(145, 585 , 200 , 20), "Inseam" , Style);

		thighs = GUI.HorizontalSlider(new Rect(80, 675, 200, 30), thighs, 1.0F, 99.0F);
        GUI.Label(new Rect(145, 650, 200, 20), "Thighs", Style);

		biceps = GUI.HorizontalSlider(new Rect(80, 740, 200, 30), biceps, 1.0F, 99.0F);
        GUI.Label(new Rect(145, 715, 200, 20), "Biceps", Style);


    }
	public void heightTest() {
		float heightDiff = (height - heightPrevious);
		heightDiff = heightDiff / 250;

		//modding arm movement after height adjustment when greater than or equal to 170cm

		/* 
		 * 
		 * DONE! Sort of
		 * 
		*/

		if (height >= 50.0f) {
			//Arm adjustments for above average height
			GameObject.Find ("Bip001 L UpperArm").transform.Translate (heightDiff * -1.0f, heightDiff * 0.3f, 0, null);
			GameObject.Find ("Bip001 R UpperArm").transform.Translate (heightDiff, heightDiff * 0.3f, 0, null);

			GameObject.Find ("Bip001 L ForeTwist").transform.Translate (0, heightDiff * 0.1f, heightDiff * 0.5f, null);
			GameObject.Find ("Bip001 R ForeTwist").transform.Translate (0, heightDiff * 0.1f, heightDiff * 0.5f, null);

			GameObject.Find ("Bip001 L ForeTwist").transform.Translate (heightDiff * 0.1f, heightDiff * 0.3f, heightDiff * 0.5f, null);
			GameObject.Find ("Bip001 R ForeTwist").transform.Translate (heightDiff * -0.1f, heightDiff * 0.3f, heightDiff * 0.5f, null);

			GameObject.Find ("Bip001 L ForeTwist2").transform.Translate (heightDiff * -0.1f, heightDiff * 0.3f, 0, null);
			GameObject.Find ("Bip001 R ForeTwist2").transform.Translate (heightDiff * -0.1f, heightDiff * 0.3f, 0, null);

			GameObject.Find ("Bip001 L ForeTwist2").transform.Translate (heightDiff * 0.1f, heightDiff * 0.3f, 0, null);
			GameObject.Find ("Bip001 R ForeTwist2").transform.Translate (heightDiff * -0.1f, heightDiff * 0.3f, 0, null);

			GameObject.Find ("Bip001 L ForeTwist2").transform.Rotate (0, 0, heightDiff * -125.0f);
			GameObject.Find ("Bip001 R ForeTwist2").transform.Rotate (0, 0, heightDiff * -125.0f);

			GameObject.Find ("Bip001 L Hand").transform.Translate (heightDiff * 0.3f, heightDiff * 0.7f, 0, null);
			GameObject.Find ("eip001 R Hand").transform.Translate (heightDiff * -0.3f, heightDiff * 0.7f, 0, null);


		} else if (height < 50) {
			//modding arm movement after height adjustment when less than 170cm
			GameObject.Find ("Bip001 L UpperArm").transform.Translate (heightDiff * -1.2f, heightDiff * 0.3f, 0, null);
			GameObject.Find ("Bip001 R UpperArm").transform.Translate (heightDiff * 1.2f, heightDiff * 0.3f, 0, null);
            
			GameObject.Find ("Bip001 L ForeTwist").transform.Translate (heightDiff * 0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);
			GameObject.Find ("Bip001 R ForeTwist").transform.Translate(heightDiff * -0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);

            GameObject.Find ("Bip001 L ForeTwist").transform.Translate (heightDiff * 0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);
			GameObject.Find ("Bip001 R ForeTwist").transform.Translate (heightDiff * -0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);

            GameObject.Find ("Bip001 L ForeTwist2").transform.Translate (heightDiff * 0.2f, heightDiff * 0.3f, heightDiff * 0.1f, null);
			GameObject.Find ("Bip001 R ForeTwist2").transform.Translate (heightDiff * -0.2f, heightDiff * 0.3f, heightDiff * 0.1f, null);

			GameObject.Find ("Bip001 L ForeTwist2").transform.Translate (heightDiff * 0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);
			GameObject.Find ("Bip001 R ForeTwist2").transform.Translate (heightDiff * -0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);

			GameObject.Find ("Bip001 L Hand").transform.Translate (heightDiff * 0.1f, heightDiff * 0.7f, heightDiff * 0.25f, null);
			GameObject.Find ("eip001 R Hand").transform.Translate (heightDiff * -0.1f, heightDiff * 0.7f, heightDiff * 0.25f, null);

            
            GameObject.Find("Bip001 L Hand").transform.Translate(heightDiff * -0.2f, heightDiff * -0.3f, heightDiff * -0.2f, Space.Self);
            GameObject.Find("eip001 R Hand").transform.Translate(heightDiff * -0.2f, heightDiff * -0.3f, heightDiff * 0.2f, Space.Self);

            GameObject.Find("Bip001 L ForeTwist2").transform.Translate(0, heightDiff * -0.3f, 0, Space.Self);
            GameObject.Find("Bip001 R ForeTwist2").transform.Translate(0, heightDiff * -0.3f, 0, Space.Self);

            GameObject.Find("Bip001 L ForeTwist1").transform.Translate(0, heightDiff * 0.2f, 0, Space.Self);
            GameObject.Find("Bip001 R ForeTwist1").transform.Translate(0, heightDiff * 0.2f, 0, Space.Self);
            
        }

		heightPrevious = height;
	}

    

	// Update is called once per frame
	void Update () {
		//Change weighting on blendshape variables
		skinMeshRenderer.SetBlendShapeWeight(0,height);
		skinMeshRenderer.SetBlendShapeWeight(1,neck);
		skinMeshRenderer.SetBlendShapeWeight(2,shoulders);
		skinMeshRenderer.SetBlendShapeWeight(3,chest);
		skinMeshRenderer.SetBlendShapeWeight(4,waist);
		skinMeshRenderer.SetBlendShapeWeight(5,hips);
		skinMeshRenderer.SetBlendShapeWeight(6,inseam);
		skinMeshRenderer.SetBlendShapeWeight(7, thighs);
		skinMeshRenderer.SetBlendShapeWeight(8, biceps);




		if (height != heightPrevious) {
			heightTest();
			
		}
	


	}

}
