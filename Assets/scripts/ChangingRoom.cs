﻿using UnityEngine;
using System.Collections;

public class ChangingRoom : MonoBehaviour {

	private int _charModelIndex = 0;

	private CharacterAsset ca;
	public string nameString;

	public UserData[] dataArray;



	private string _charModelName = "Male";

	// Use this for initialization
	void Start () {
		

		ca = GameObject.Find ("Character Asset Manager").GetComponent<CharacterAsset> ();

		InstantiateCharacterModel ();
	}


	void OnGUI(){
	
		ChangeCharacterMesh ();


        if (GUI.Button(new Rect(80, 780, 200, 40), "Save")){
            if(_charModelName.Equals("Male")){
				//dataArray[dataArray.Length] = new UserData(BlendshapeSlider.height, , true);
                

            }
			else{
				//dataArray[dataArray.Length] = new UserData(false, BlendshapeFemale.height, BlendshapeFemale.neck, BlendshapeFemale.shoulders, BlendshapeFemale.waist, BlendshapeFemale.hips, 
					//BlendshapeFemale.inseam, BlendshapeFemale.thighs, BlendshapeFemale.overBust, BlendshapeFemale.underBust);
			}
        }
	}


	private void ChangeCharacterMesh() {
	
		if (GUI.Button (new Rect (80, 150, 200, 40), _charModelName)) {
		
			_charModelIndex++;

			InstantiateCharacterModel ();
		
		}
	
	}


	// Update is called once per frame
	void Update () {
	
	}

	private void InstantiateCharacterModel() {

		switch (_charModelIndex) {

		case 1:
			_charModelName = "Female";

			break;
		default:
			_charModelName = "Male";
			_charModelIndex = 0;


			break;

		}

		if (transform.childCount > 0) {
		
			for (int cnt = 0; cnt < transform.childCount; cnt++) {
			
				Destroy (transform.GetChild(cnt).gameObject);
			}
		}

		GameObject model  = Instantiate(ca.CharacterMesh[_charModelIndex] , transform.position , Quaternion.identity) as GameObject ;

		model.transform.parent = transform;

		model.transform.rotation = transform.rotation;
	}
}
